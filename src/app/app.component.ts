import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  title
} from 'process';
import {
  NzNotificationService
} from 'ng-zorro-antd';
import {
  HttpClient
} from '@angular/common/http';

interface DataItem {
  symbol: string;
  price: string;
  lastChange ? : number;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit {
  title = 'test-avolox';
  symbolsForm: FormGroup;
  modalVisible: boolean = false;
  searchValue = '';
  visible = false;

  listOfData: DataItem[] = [
    
  ];
  listOfDisplayData = [];

  constructor(private notification: NzNotificationService,
    private httpClient: HttpClient) {}

  ngOnInit() {
    this.symbolsForm = new FormGroup({
      symbol: new FormControl(null, Validators.required),
    });
    if(localStorage.getItem('data')!==null){
      this.listOfDisplayData = [...JSON.parse(localStorage.getItem('data'))];
    }
    

    setInterval(() => {
      if (this.listOfData.length > 0) {
        for (let i = 0; i < this.listOfData.length; i++) {
          this.httpClient.get < DataItem > (`https://api.binance.com/api/v1/ticker/price?symbol=${this.listOfData[0].symbol}`).subscribe(
            data => {
              const newPrice = +data.price
              const diff = +this.listOfData[i].price - newPrice;
              this.listOfData[i].lastChange = diff;
              if (diff > 0) {
                document.getElementById("color").style.backgroundColor = "green";
                setTimeout(() => {
                  document.getElementById("color").style.backgroundColor = "white";
                }, 2000)
              }
              if (diff < 0) {
                document.getElementById("color").style.backgroundColor = "red";
                setTimeout(() => {
                  document.getElementById("color").style.backgroundColor = "white";
                }, 2000)

              }
          
            })

        }
        localStorage.setItem('data',JSON.stringify(this.listOfData));
        this.listOfDisplayData = [...JSON.parse(localStorage.getItem('data'))];
      }
    }, 5000);

  }

  reset(): void {
    this.searchValue = '';
    this.search();
  }

  search(): void {
    this.visible = false;
    this.listOfDisplayData = this.listOfData.filter((item: DataItem) => item.symbol.indexOf(this.searchValue) !== -1);
  }

  deleteRow(data: DataItem) {
    const index = this.listOfData.findIndex(item => {
      item.symbol === data.symbol
    });
    this.listOfData.splice(index, 1);
    this.listOfDisplayData = this.listOfData;
  }

  addSymbol() {
    this.modalVisible = true;
  }

  handleCancel() {
    this.modalVisible = false;
  }

  onSubmit() {
    const tempData = this.listOfData.filter((item: DataItem) => item.symbol.indexOf(this.symbolsForm.controls["symbol"].value) !== -1);
   
    if (tempData.length == 0) {
      this.httpClient.get < DataItem > (`https://api.binance.com/api/v1/ticker/price?symbol=${this.symbolsForm.controls["symbol"].value}`).subscribe(
        data => {
          this.listOfData.push({
            symbol: data.symbol,
            price: data.price,
            lastChange: 0
          });
          localStorage.setItem('data',JSON.stringify(this.listOfData));
          this.listOfDisplayData = [...JSON.parse(localStorage.getItem('data'))];
     
        }, err => {
          this.notification.create(
            'error',
            `The symbol could not be added in the table, may be the symbol does not exist in the server`,
            ''
          );
        }
      );
    } else {
      this.notification.create(
        'error',
        `The symbol already exist in the table`,
        ''
      );
    }


  }
}
